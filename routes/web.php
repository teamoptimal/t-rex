<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/products', function () {
    return view('products');
});

Route::get('/services', function () {
    return view('services');
});

Route::get('/contact', function () {
    return view('contact');
});

Route::get('/search', function () {
    return view('search');
});


/* MAIL SEND */
Route::post('/send', 'MailController@send');

// CMS
// LOGIN TO CMS
Route::get('/login', function () {
    return view('/cms/login');
});

// VIEW CMS HOME
Route::get('/cmshome', function () {
    return view('/cms/cmshome');
});

// VIEW HOME PAGE DETAILS
Route::get('/homepagedetails', function () {
    return view('/cms/homepagedetails');
});

// VIEW ABOUT PAGE DETAILS
Route::get('/aboutpagedetails', function () {
    return view('/cms/aboutpagedetails');
});

// VIEW PRODUCTS PAGE
Route::get('/productspagedetails', function () {
    return view('/cms/productspagedetails');
});

// VIEW SERVICES PAGE
Route::get('/servicespagedetails', function () {
    return view('/cms/servicespagedetails');
});

// VIEW TEAM MEMBERS
Route::get('/teammembersview', function () {
    return view('/cms/teammembersview');
});

// ADD TEAM MEMBER
Route::get('/teammembersadd', function () {
    return view('/cms/teammembersadd');
});

// EDIT TEAM MEMBER
Route::get('/teammembersedit/{card}', function ($card) {
    if (session()->has('loggedIn')) {
        if (session('loggedIn')) {
            return view('/cms/teammembersedit', ["card" => $card]);
        } else {
            return Redirect('/dologout'); 
        }
    } else {
        return Redirect('/dologout'); 
    } 
});

// VIEW PRINCIPLES
Route::get('/principlesview', function () {
    return view('/cms/principlesview');
});

// ADD PRINCIPLES
Route::get('/principlesadd', function () {
    return view('/cms/principlesadd');
});

// EDIT PRINCIPLE
Route::get('/principlesedit/{card}', function ($card) {
    if (session()->has('loggedIn')) {
        if (session('loggedIn')) {
            return view('/cms/principlesedit', ["card" => $card]);
        } else {
            return Redirect('/dologout'); 
        }
    } else {
        return Redirect('/dologout'); 
    } 
});

// VIEW PRODUCTS
Route::get('/productsview', function () {
    return view('/cms/productsview');
});

// ADD PRODUCTS
Route::get('/productsadd', function () {
    return view('/cms/productsadd');
});

// EDIT PRODUCTS
Route::get('/productsedit/{card}', function ($card) {
    if (session()->has('loggedIn')) {
        if (session('loggedIn')) {
            return view('/cms/productsedit', ["card" => $card]);
        } else {
            return Redirect('/dologout'); 
        }
    } else {
        return Redirect('/dologout'); 
    } 
});


// VIEW SERVICES
Route::get('/servicesview', function () {
    return view('/cms/servicesview');
});

// ADD SERVICES
Route::get('/servicesadd', function () {
    return view('/cms/servicesadd');
});

// EDIT SERVICES
Route::get('/servicesedit/{card}', function ($card) {
    if (session()->has('loggedIn')) {
        if (session('loggedIn')) {
            return view('/cms/servicesedit', ["card" => $card]);
        } else {
            return Redirect('/dologout'); 
        }
    } else {
        return Redirect('/dologout'); 
    } 
});

// OPERATIONS

// PERFORM LOGIN OPERATION
Route::post('/dologin', 'LoginController@login');

// PERFORM LOGOUT OPERATION
Route::get('/dologout', 'LoginController@logout');

//PERFORM HOME PAGE DETAILS EDIT
Route::post('/dohomepagedetailsedit', 'LoginController@dohomepagedetailsedit');

//PERFORM ABOUT PAGE DETAILS EDIT
Route::post('/doaboutpagedetailsedit', 'LoginController@doaboutpagedetailsedit');

//PERFORM PRODUCTS PAGE DETAILS EDIT
Route::post('/doproductspagedetailsedit', 'LoginController@doproductspagedetailsedit');

//PERFORM SERVICES PAGE DETAILS EDIT
Route::post('/doservicespagedetailsedit', 'LoginController@doservicespagedetailsedit');

//PERFORM TEAM MEMBER ADD
Route::post('/doteammemberadd', 'LoginController@doteammemberadd');

// PERFORM TEAM MEMBER EDIT OPERATION
Route::post('/doteammemberedit/{card}', ['uses' => 'LoginController@doteammemberedit']);

// PERFORM TEAM MEMBER DELETE OPERATION
Route::get('/doteammemberdelete/{card}', ['uses' => 'LoginController@doteammemberdelete']);

//PERFORM PRINCIPLE ADD
Route::post('/doprincipleadd', 'LoginController@doprincipleadd');

// PERFORM PRINCIPLE EDIT OPERATION
Route::post('/doprincipleedit/{card}', ['uses' => 'LoginController@doprincipleedit']);

// PERFORM PRINCIPLE DELETE OPERATION
Route::get('/doprinciplesdelete/{card}', ['uses' => 'LoginController@doprinciplesdelete']);

//PERFORM PRODUCTS ADD
Route::post('/doproductadd', 'LoginController@doproductadd');

// PERFORM PRODUCTS EDIT OPERATION
Route::post('/doproductedit/{card}', ['uses' => 'LoginController@doproductedit']);

// PERFORM PRODUCTS DELETE OPERATION
Route::get('/doproductsdelete/{card}', ['uses' => 'LoginController@doproductsdelete']);

//PERFORM SERVICES ADD
Route::post('/doserviceadd', 'LoginController@doserviceadd');

// PERFORM SERVICES EDIT OPERATION
Route::post('/doserviceedit/{card}', ['uses' => 'LoginController@doserviceedit']);

// PERFORM SERVICES DELETE OPERATION
Route::get('/doservicesdelete/{card}', ['uses' => 'LoginController@doservicesdelete']);









