<?php 

    // PRODUCTS AGE DETAILS
    $productsDetails = DB::table('productspagedetails')->get();

    // PRODUCTS
    $products = DB::table('products')->get();

?>

<!doctype html>
<html lang="{{ app()->getLocale() }}">
    
@include('includes/header')

    <body>
       
        <div div="wrapper">

            @include('includes/menu')

            <!-- JQUERY -->
            <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

            <?php $curPage = "products"; ?>

            <?php switch($curPage) {

                case "products":
                ?>
                <script>
                    var menuEL = $("#productsMenu");
                    menuEL.css('color', '#ee3126');
                    var offset = menuEL.offset();
                    var offsetLeft = menuEL.width() / 4;
                    $("#menuIcon").css("visibility", "visible");
                    $('#menuIcon').offset( {top: offset.top - 50, left: offset.left - ($("#menuIcon").width() / 2) + offsetLeft});
                </script>
                <?php
                break;
            }?>

            <div id="homeHeader">

                <img id="homeHeaderImage" src="<?php echo $productsDetails[0]->imgURL; ?>">

                <div id="homeHeaderText">

                    <h2><?php echo $productsDetails[0]->headerText; ?></h2>

                </div>


                <div id="homeHeaderEnquire">

                    <a href="/contact"><div class="homeHeaderButton">ENQUIRE NOW</div></a>

                </div>

            </div>

            <div id="productsContent">

                <div id="productsOurRange">

                    <h2 id="productsOurRangeHeader">Our Product Range</h2>

                    <div class="centerUnderlineYellow"> </div>

                    <h3 id="productsOurRangeText"><?php echo $productsDetails[0]->productRangeHeaderText; ?></h3>

                </div>

                 <?php foreach ($products as $product) { ?>

                    <div class="productsProduct" id="pr<?php echo $product->id; ?>">

                        <div class="productProductImageParent productImageDesktop <?php echo ($product->id % 2 == "0") ? "floatRightDesktop" : "floatLeftDesktop"; ?>">

                            <img class="productsProductImage" src="<?php echo $product->imgURLPrimary; ?>" alt="<?php echo $product->productName; ?>">

                        </div>

                        <div class="productsProductDetail <?php echo ($product->id % 2 == "0") ? "floatLeftDesktop" : "floatRightDesktop"; ?>">

                            <h4 class="productsProductHeader"><?php echo $product->productName; ?></h4>

                            <?php if ($product->id == "1") { ?>

                                <img id="jetstreamLogoMobile" src="{{asset('images/icons/Jetstream-Logo.png')}}">

                            <?php } ?>

                            <img class="productsProductImage productImageMobile" src="<?php echo $product->imgURLPrimary; ?>" alt="<?php echo $product->productName; ?>">

                            <?php if ($product->productApplicationText != "na" && $product->productApplicationText != "") { ?>

                                <h5 class="productsProductDetailHeader">Application</h5>

                                <h6 class="productsProductDetailText"><?php echo $product->productApplicationText; ?></h6>

                            <?php } ?>

                            <?php if ($product->productDescriptionText != "na" && $product->productDescriptionText != "") { ?>

                                <h5 class="productsProductDetailHeader">Description</h5>

                                <h6 class="productsProductDetailText"><?php echo $product->productDescriptionText; ?></h6>

                            <?php } ?>

                            <?php if ($product->productBenefitsText != "na" && $product->productBenefitsText != "") { ?>

                                <h5 class="productsProductDetailHeader">Price</h5>

                                <h6 class="productsProductDetailText"><?php echo $product->productBenefitsText; ?></h6>

                            <?php } ?>

                            <?php if ($product->productFeaturesText != "na" && $product->productFeaturesText != "") { ?>
                        
                                <h5 class="productsProductDetailHeader">Features</h5>

                                <h6 class="productsProductDetailText"><?php echo $product->productFeaturesText; ?></h6>

                            <?php } ?>

                            <?php if ($product->id == "1") { ?>

                                <img id="jetstreamLogo" src="{{asset('images/icons/Jetstream-Logo.png')}}">

                            <?php } ?>

                            <div class="clearfix"></div>

                        </div>

                        <div class="clearfix"></div>

                    </div>

                <?php } ?>


            </div>

            <?php $curPage = "products"; ?>
            
            @include('includes/footer')