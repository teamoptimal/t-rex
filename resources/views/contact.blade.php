<!doctype html>
<html lang="{{ app()->getLocale() }}">
    
@include('includes/header')

    <body>
       
        <div div="wrapper">

            @include('includes/menu')

            <!-- JQUERY -->
            <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

            <?php $curPage = "contact"; ?>

            <?php switch($curPage) {

                case "contact":
                ?>
                <script>
                    var menuEL = $("#contactMenu");
                    menuEL.css('color', '#ee3126');
                    var offset = menuEL.offset();
                    var offsetLeft = menuEL.width() / 4;
                    $("#menuIcon").css("visibility", "visible");
                    $('#menuIcon').offset( {top: offset.top - 50, left: offset.left - ($("#menuIcon").width() / 2) + offsetLeft});
                </script>
                <?php
                break;

            }?>

            <div id="contactHeader">

            </div>

            <div id="contactContent">

                <div id="contactContactUs">

                    <h2 id="contactContactUsHeader">Contact Us</h2>

                    <div class="centerUnderlineYellow"> </div>

                    <h3 id="contactContactUsText">Contact us today with any questions you may have or request a free quote.</h3>

                </div>

                <div id="contactSection">

                    <div id="contactSectionLeft">

                        <div class="contactSectionLeftDetail">

                            <div class="contactSectionLeftDetailTop">
                                <h4 class="contactSectionLeftDetailHeader"><img class="contactSectionLeftDetailImageHidden hidden">Office/Workshop</h4>
                            </div>

                            <div class="contactSectionLeftDetailBottom">
                                <h5 class="contactSectionLeftDetailText"><div class="contactSectionLeftDetailImageParent"><img class="contactSectionLeftDetailImage" src="{{asset('images/icons/CallIcon.png')}}"></div>082 337 7678</h4>
                            </div>

                        </div>

                        <div class="contactSectionLeftDetail">

                            <!-- <div class="contactSectionLeftDetailTop">
                                <h4 class="contactSectionLeftDetailHeader"><img class="contactSectionLeftDetailImageHidden hidden">Gero Lϋtge</h4>
                            </div> -->

                            <!-- <div class="contactSectionLeftDetailBottom">
                                <h5 class="contactSectionLeftDetailText"><div class="contactSectionLeftDetailImageParent"><img class="contactSectionLeftDetailImage" src="{{asset('images/icons/PhoneIcon.png')}}"></div>079 289 8833</h4>
                                <h5 class="contactSectionLeftDetailText"><div class="contactSectionLeftDetailImageParent"><img class="contactSectionLeftDetailImage" src="{{asset('images/icons/EmailIcon.png')}}"></div>gero@trexmachinery.co.za</h4>
                            </div> -->

                        </div>

                        <div class="contactSectionLeftDetail">

                            <div class="contactSectionLeftDetailTop">
                                <h4 class="contactSectionLeftDetailHeader"><img class="contactSectionLeftDetailImageHidden hidden">Steve Stopforth</h4>
                            </div>

                            <div class="contactSectionLeftDetailBottom">
                                <h5 class="contactSectionLeftDetailText"><div class="contactSectionLeftDetailImageParent"><img class="contactSectionLeftDetailImage" src="{{asset('images/icons/EmailIcon.png')}}"></div>steve@trexmachinery.co.za</h4>
                            </div>

                        </div>

                    </div>

                    <div id="contactSectionRight">

                        <form method="post" action="/send" id="contactform">

                            <div id="contactComplete">
                                <i id="contactCompleteClose" class="fas fa-times" onclick="this.parentElement.style.display = 'none'"></i>
                                <h3>THANK YOU!</h3>
                                <h4>Your message has been sent.</h4>
                            </div>

                        {{csrf_field()}}

                            <div class="contactFormSection floatLeft">

                                <label class="contactFormLabel">FIRST NAME *</label><br>
                                <input type="text" name="firstname" class="contactFormInput" id="contactFormFirstName"></input>

                            </div>

                            <div class="contactFormSection floatRight">

                                <label class="contactFormLabel">LAST NAME *</label><br>
                                <input type="text" name="lastname" class="contactFormInput" id="contactFormLastName"></input>

                            </div>

                            <div class="clearfix"></div>

                            <div class="contactFormSection floatLeft">

                                <label class="contactFormLabel">EMAIL ADDRESS *</label><br>
                                <input type="text" name="email" class="contactFormInput" id="contactFormEmail"></input>

                            </div>

                            <div class="contactFormSection floatRight">

                                <label class="contactFormLabel">PHONE NUMBER *</label><br>
                                <input type="text" name="number" class="contactFormInput" id="contactFormPhone"></input>

                            </div>

                            <div class="clearfix"></div>

                            <div class="contactFormSectionFull">
                                    
                                    <label class="contactFormLabel">COMPANY NAME *</label><br>
                                    <input type="text" name="companyname" class="contactFormInputFull" id="contactFormCompany"></input>
    
                                </div>

                            <div class="contactFormSectionFull">
                                    
                                <label class="contactFormLabel">SUBJECT</label><br>
                                <input type="text" name="subject" class="contactFormInputFull" id="contactFormSubject"></input>

                            </div>

                            <div class="contactFormSectionFull">

                                <label class="contactFormLabel">HOW CAN WE HELP?</label><br>
                                <textarea name="usermessage" id="contactFormText"></textarea>

                            </div>

                            <button id="standardButton" class="contactFormButton"> SUBMIT </button>

                            <div class="clearfix"></div>

                        </form>

                    </div>

                    <div class="clearfix"></div>

                </div>

            </div>

            <?php $curPage = "contact"; ?>
            
            @include('includes/footer')