<?php 

// GET HOME DETAILS
$homeDetails = DB::table('homepagedetails')->get();

// GET PRODUCTS
$homeProducts = DB::table('products')->get();

// GET SERVICES
$homeServices = DB::table('services')->get();

// GET DIFFERENTIATORS
$homeDifferentiators = DB::table('keydifferentiators')->get();

?>

<!doctype html>
<html lang="{{ app()->getLocale() }}">
    
@include('includes/header')

    <body>
       
        <div div="wrapper">

            @include('includes/menu')
            <!-- JQUERY -->
            <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
                        
            <?php $curPage = "home"; ?>

            <?php switch($curPage) {

            case "home":
            ?>
            <script>
                var menuEL = $("#homeMenu");
                menuEL.css('color', '#ee3126');
                var offset = menuEL.offset();
                var offsetLeft = menuEL.width() / 4;
                $("#menuIcon").css("visibility", "visible");
                $('#menuIcon').offset( {top: offset.top - 50, left: offset.left - ($("#menuIcon").width() / 2) + offsetLeft});
            </script>
            <?php
            break;
            }?>


            <div id="homeHeader">

                <img id="homeHeaderImage" src="<?php echo $homeDetails[0]->imgURL; ?>">

                <div id="homeHeaderText">

                    <h2><?php echo $homeDetails[0]->headerText; ?></h2>

                </div>
                

                <div id="homeHeaderEnquire">

                    <a href="/contact"><div class="homeHeaderButton">ENQUIRE NOW</div></a>

                </div>

            </div>

            <div id="homeContent">

                <div id="homeProducts">

                    <h2 id="homeProductsHeader">Products</h2>

                    <div class="centerUnderlineYellow"> </div>

                    <h3 id="homeProductsText"><?php echo $homeDetails[0]->productsHeaderText; ?></h3>

                    <?php foreach ($homeProducts as $product) { ?>

                    <div class="homeProduct">

                        <a href="/products#pr<?php echo $product->id; ?>"><img src="<?php echo $product->imgURLHome; ?>" alt="<?php echo $product->productName; ?>"></a>

                        <h4 class="homeProductText"><?php echo $product->productName; ?></h4>

                        <a href="/products#pr<?php echo $product->id; ?>"><div class="homeProductButton">View Product</div></a>

                    </div>

                    <?php } ?>

                </div>

                <div id="homeServices">

                    <h2 id="homeServicesHeader">Services</h2>

                    <div class="centerUnderlineYellow"> </div>

                    <h3 id="homeServicesText"><?php echo $homeDetails[0]->servicesHeaderText; ?></h3>

                    <?php foreach ($homeServices as $service) { ?>

                    <div class="homeService">

                        <a href="/services#se<?php echo $service->id; ?>"><img src="<?php echo $service->imgURLSecondary; ?>" alt="<?php echo $service->serviceName; ?>"></a>

                        <h4 class="homeServiceText"><?php echo $service->serviceName; ?></h4>

                        <a href="/services#se<?php echo $service->id; ?>"><div class="homeServiceButton">Read More</div></a>

                    </div> </a>

                    <?php } ?>

                </div>

                <div id="homeDifferentiators">

                    <h2 id="homeDifferentiatorsHeader">Key Differentiators</h2>

                    <div class="centerUnderlineYellow"></div>

                    <h3 id="homeDifferentiatorsText"><?php echo $homeDetails[0]->differentiatorsHeaderText; ?></h3>

                    <?php foreach ($homeDifferentiators as $dif) { ?>

                    <div class="homeDifferentiator">

                        <img class="homeDifferentiatorImage" src="<?php echo $dif->imgURL; ?>" alt="<?php echo $dif->differentiatorName; ?>">

                        <h4 class="homeDifferentiatorHeader"><?php echo $dif->differentiatorName; ?></h4>

                        <div class="centerUnderlineGrey"> </div>

                        <h5 class="homeDifferentiatorText"><?php echo $dif->differentiatorText; ?></h5>

                    </div>

                    <?php } ?>

                </div>

                <!-- JQUERY -->
            <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

            </div>
            
            @include('includes/footer')