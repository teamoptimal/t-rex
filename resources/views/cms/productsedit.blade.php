<?php 

session_start();

if (session('loggedIn') != true) {

    session_destroy();
    header('Location: /login');
    die();
    
}

$pageTitle = "Product Edit"; 

$productsCard = DB::table('products')->where('id', $card)->get();

?>

@extends('layouts.cmsheader')

@include('layouts.cmsinclude')

    <!-- Page Content -->
    <div class="right_col" style="min-height: 100% !important;">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                <h2>Product <small> Edit </small></h2>
                
                <div class="clearfix"></div>
                </div>
                <div class="x_content">
                <br>
                <form id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" method="post" action="/doproductedit/<?php echo $productsCard[0]->id; ?>" enctype="multipart/form-data">

                   {{csrf_field()}}

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="imgdesc"> Product Name <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="imgdesc" name="productname" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $productsCard[0]->productName; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Product Application<span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea class="form-control" rows="6" name="productapplication"><?php echo $productsCard[0]->productApplicationText; ?></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Product Description<span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea class="form-control" rows="6" name="productdescription"><?php echo $productsCard[0]->productDescriptionText; ?></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Product Price<span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea class="form-control" rows="6" name="productbenefits"><?php echo $productsCard[0]->productBenefitsText; ?></textarea>
                        </div>
                    </div>

                     <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Product Features<span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea class="form-control" rows="6" name="productfeatures"><?php echo $productsCard[0]->productFeaturesText; ?></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="img"> Product Primary Image (Shown on Products Page. Leave blank to not update.)<span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6">
                            <input type="file" id="img" accept="image/*" name="img1" class="form-control col-md-7 col-xs-12" style="border: none !important;">
                        </div>
                    </div>

                     <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="img"> Product Secondary Image (Shown on Home Page. Leave blank to not update.)<span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6">
                            <input type="file" id="img" accept="image/*" name="img2" class="form-control col-md-7 col-xs-12" style="border: none !important;">
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
                            <a href="/principlesview" class="btn btn-primary">Cancel</a>
                            <button class="btn btn-primary" type="reset">Reset</button>
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </div>

                    
                </form>

                </div>
            </div>
        </div>

    </div>

@extends('layouts.cmsfooter')
