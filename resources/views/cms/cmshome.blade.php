<?php 

session_start();

if (session('loggedIn') != true) {

    session_destroy();
    header('Location: /login');
    die();
    
}

$pageTitle = "Home"; ?>

@extends('layouts.cmsheader')

@include('layouts.cmsinclude')

    <!-- page content -->
    <div class="right_col" style="min-height: 100% !important;">
    </div>

@extends('layouts.cmsfooter')
