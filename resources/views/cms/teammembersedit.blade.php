<?php 

session_start();

if (session('loggedIn') != true) {

    session_destroy();
    header('Location: /login');
    die();
    
}

$pageTitle = "Team Member Edit"; 

// GET CARD RECORDS
$teamMemberCards = DB::table('teammembers')->where('id', $card)->get();

?>

@extends('layouts.cmsheader')

@include('layouts.cmsinclude')

    <!-- Page Content -->
    <div class="right_col" style="min-height: 100% !important;">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                <h2>Team Member <small> Edit </small></h2>
                
                <div class="clearfix"></div>
                </div>
                <div class="x_content">
                <br>
                <form id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" method="post" action="/doteammemberedit/<?php echo $teamMemberCards[0]->id; ?>" enctype="multipart/form-data">

                   {{csrf_field()}}

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="imgdesc"> Member Name <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="imgdesc" name="teammembername" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $teamMemberCards[0]->teamMemberName; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="imgdesc"> Member Position <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="imgdesc" name="teammemberposition" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $teamMemberCards[0]->teamMemberPosition; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Member Bio <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea class="form-control" rows="6" name="teammembertext" required="required"><?php echo $teamMemberCards[0]->teamMemberText; ?></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="img"> Member Image (Leave blank to not update.)<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6">
                            <input type="file" id="img" accept="image/*" name="img" class="form-control col-md-7 col-xs-12" style="border: none !important;">
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
                            <a href="/teammembersview" class="btn btn-primary">Cancel</a>
                            <button class="btn btn-primary" type="reset">Reset</button>
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </div>

                    
                </form>

                </div>
            </div>
        </div>

    </div>

@extends('layouts.cmsfooter')
