<?php 

session_start();

if (session('loggedIn') != true) {

    session_destroy();
    header('Location: /login');
    die();
    
}

$pageTitle = "Principle Edit"; 

$principlesCard = DB::table('principles')->where('id', $card)->get();

?>

@extends('layouts.cmsheader')

@include('layouts.cmsinclude')

    <!-- Page Content -->
    <div class="right_col" style="min-height: 100% !important;">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                <h2>Principle <small> Edit </small></h2>
                
                <div class="clearfix"></div>
                </div>
                <div class="x_content">
                <br>
                <form id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" method="post" action="/doprincipleedit/<?php echo $principlesCard[0]->id; ?>" enctype="multipart/form-data">

                   {{csrf_field()}}

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="imgdesc"> Principle Name <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="imgdesc" name="principlename" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $principlesCard[0]->principleName; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="img"> Principle Image (Leave blank to not update.)<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6">
                            <input type="file" id="img" accept="image/*" name="img" class="form-control col-md-7 col-xs-12" style="border: none !important;">
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
                            <a href="/principlesview" class="btn btn-primary">Cancel</a>
                            <button class="btn btn-primary" type="reset">Reset</button>
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </div>

                    
                </form>

                </div>
            </div>
        </div>

    </div>

@extends('layouts.cmsfooter')
