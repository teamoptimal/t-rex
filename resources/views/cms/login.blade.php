<!doctype html>
<html lang="{{ app()->getLocale() }}">
    
@include('includes/header')

    <body>
       
        <div div="wrapper">

            @include('includes/menu')
            <!-- JQUERY -->
            <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
                        
            <?php $curPage = "home"; ?>

            <?php switch($curPage) {

            case "home":
            ?>
            <script>
                $("#menuIcon").css("visibility", "hidden");
            </script>
            <?php
            break;
            }?>

            <div id="loginEl">
                <div id="loginHeader">ADMIN LOGIN</div>

                <div class="centerUnderlineYellow"> </div>

                <div class="card-body">
                    <form method="POST" action="/dologin">
                        {{ csrf_field() }}

                        <br>

                        <label class="loginLabel">Username</label> <br>
                        <input class="loginInput" type="text" name="username" value=""> <br> <br>

                        <label class="loginLabel">Password</label> <br>
                        <input class="loginInput" type="password" name="password" value=""> <br> <br>

                        <input id="loginSubmit" type="submit" value="LOGIN">
                        
                    </form>
                </div>
            </div>

                <!-- JQUERY -->
            <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

            </div>
            
            
