            <div id="footer">

                <div id="footerContent">

                    <div id="footerContentLeft">

                        <img id="footerContentLeftImage" src="{{asset('images/icons/FullLogo.png')}}" alt="T-REX Logo">

                        <h3 id="footerContentLeftText">T - Rex machinery is a South African company that develops, design and manufactures superior equipment for the food processing industry with emphasis on the meat industry.</h3>

                        <a href="/about"><div id="standardButton"> Learn More </div></a>

                    </div>

                    <div id="footerContentMiddle">

                        <h4 id="footerContentMiddleHeader">Contact Us</h4>

                        <p class="footerContentMiddleHeaderDetail">Tel: 082 337 7678</p>

                        <!-- <p class="footerContentMiddleHeaderDetail">Gero Lϋtge <br> 079 289 8833 <br> gero@trexmachinery.co.za</p> -->

                        <p class="footerContentMiddleHeaderDetail">Steve Stopforth <br> steve@trexmachinery.co.za</p>

                        <a href="/contact"><div id="standardButton"> Contact Us </div></a>
                        
                    </div>

                    <div id="footerContentRight">

                        <h4 id="footerContentRightHeader">Find Us</h4>

                        <iframe id="footerMap" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13250.425372978554!2d18.6978789!3d-33.8740347!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x37b10f52a0cecd39!2sT-Rex+Machinery!5e0!3m2!1sen!2sza!4v1532509840165" frameborder="0" style="border:0" allowfullscreen></iframe>

                    </div>

                    <div class="clearfix"></div>

                </div>

            </div>

        </div>

    <script src="{{asset('js/hamburgericonmenu.js')}}"></script>

    <script>

        // SEARCH
        function searchProducts(event) {

            event.preventDefault();

            // Submit using AJAX.
            $.ajax({
                type: 'GET',
                url: "{{ url('search') }}",
                data: "searchTerm=" + $("#searchInput").val().trim(),
                dataType: "json",
            }).done(function(response) {

                var searchReturn = response;

                $('#desktopSearchDropdown').show();

                $('#desktopSearchDropdown').children().remove();
                
                for (var i = 0; i < searchReturn.length; ++i) {
                    var searchDropdownEl = "<div class='desktopSearchDropdownItem'><a href='/" + searchReturn[i]['urlPre'] + "#pr" + searchReturn[i]['id'] + "'>" + searchReturn[i]['name'] + ", in " + searchReturn[i]['type'] + "</div>";
                    $("#desktopSearchDropdown").prepend(searchDropdownEl);
                }

            })

        }

        $(document).ready(function(){

            //CONTACT
            $(function() {
                // Get the form.
                var form = $('#contactform');

                // Get the messages div.
                var formSubmit = $('#contactComplete');

                // Set up an event listener for the contact form.
                $(form).submit(function(event) {

                    // Stop the browser from submitting the form.
                    event.preventDefault();

                    // Serialize the form data.
                    var formData = $(form).serialize();

                    // Submit the form using AJAX.
                    $.ajax({
                        type: 'POST',
                        url: "/send",
                        data: formData
                    }).done(function(response) {

                        // Clear the form.
                        $(".contactFormInput").each(function() {
                            $(this).val('');
                        });
                        $(".contactFormInputFull").each(function() {
                            $(this).val('');
                            $(this).html('');
                        });
                        $('#contactFormText').val('');

                        // POPUP CONTACT MESSAGE
                        formSubmit.show();

                    })
                }); 
            });

        });

    </script>

    </body>
</html>
