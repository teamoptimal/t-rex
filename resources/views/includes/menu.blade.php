<div id="header">

    <div id="headerMobile">

        <!-- HAMBURGER MENU -->
        <div id="hamburgericonmenuwrapper">
            <div id="hamburgerui">
                <ul>
                    <li><a href="#"><span id="navtoggler"></span></a></li>
                </ul>
            </div>
            <nav id="fullscreenmenu">
                <ul>
                    <li style="border: none !important;"><a href="/">Home</a></li>
                    <li><a href="/about">ABOUT US</a></li>
                    <li><a href="/products">PRODUCTS</a></li>
                    <li><a href="/services">SERVICES</a></li>
                    <li><a href="/contact">CONTACT</a></li>
                </ul>
            </nav>
        </div>

        <div id="headerLogo">

            <img src="{{asset('images/icons/FullLogo.png')}}" alt="T-Rex Machinery">
            <h1 class="hidden absoluteTop">T-Rex Machinery</h1>

        </div>

        <div id="social">

            <a href="https://www.facebook.com/T-REX-Machinery-1051023671583967/" target="_blank"><img class="menuSocialIcon" src="{{asset('images/icons/facebookIcon.png')}}"></a>

            <a href="/contact"><img class="menuSocialIcon" src="{{asset('images/icons/EmailIcon.png')}}"></a>

            <img class="menuSocialIcon" src="{{asset('images/icons/searchicon.png')}}" onclick="searchPopUp()">

        </div>

    </div>

    <div id="headerDesktop">

        <div id="headerLogoDesktop">

            <img src="{{asset('images/icons/FullLogo.png')}}" alt="T-Rex Machinery">
            <h1 class="hidden absoluteTop">T-Rex Machinery</h1>

        </div>

        <div id="headerDesktopContent">

            <div id="headerDesktopContentTop">

                <form action="" method="get">

                <input name="search" id="searchInput" placeholder="Search this site...">

                <div id="desktopSearchDropdown">
                </div>

                <button class="standardButton searchButton" onclick="searchProducts(event)">Search</button>
                    
                </form>

            </div>

            <div id="headerDesktopContentBottom">

                <nav id="headerDesktopNav">

                    <ul>

                        <li><a id="homeMenu" href="/">HOME</a></li>
                        <li><a id="aboutMenu" href="/about">ABOUT US</a></li>
                        <li><a id="productsMenu" href="/products">PRODUCTS</a></li>
                        <li><a id="servicesMenu" href="/services">SERVICES</a></li>
                        <li><a id="contactMenu" href="/contact">CONTACT</a></li>

                    </ul>

                    <img id="menuIcon" src="{{asset('images/menuIcon.png')}}">

                    <a href="https://www.facebook.com/T-REX-Machinery-1051023671583967/" target="_blank"><img style="z-index: 5;" class="menuSocialIconDesktop" src="{{asset('images/icons/facebookIcon.png')}}"></a>

                    <a href="/contact"><img class="menuSocialIconDesktop" src="{{asset('images/icons/EmailIcon.png')}}"></a>

                </nav>

            </div>

            <div class="clearfix"></div>

        </div>

        <div class="clearfix"></div>

    </div>

</div>