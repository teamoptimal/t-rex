<?php 

    // SERVICES AGE DETAILS
    $servicesDetails = DB::table('servicespagedetails')->get();

    // SERVICES
    $services = DB::table('services')->get();

?>

<!doctype html>
<html lang="{{ app()->getLocale() }}">
    
@include('includes/header')

    <body>
       
        <div div="wrapper">

            @include('includes/menu')

            <!-- JQUERY -->
            <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

            <?php $curPage = "services"; ?>

            <?php switch($curPage) {
                case "services":
                ?>
                <script>
                    var menuEL = $("#servicesMenu");
                    menuEL.css('color', '#ee3126');
                    var offset = menuEL.offset();
                    var offsetLeft = menuEL.width() / 4;
                    $("#menuIcon").css("visibility", "visible");
                    $('#menuIcon').offset( {top: offset.top - 50, left: offset.left - ($("#menuIcon").width() / 2) + offsetLeft});
                </script>
                <?php
                break;

            }?>

           <div id="homeHeader">

                <img id="homeHeaderImage" src="<?php echo $servicesDetails[0]->imgURL; ?>">

                <div id="homeHeaderText">

                    <h2><?php echo $servicesDetails[0]->headerText; ?></h2>

                </div>


                <div id="homeHeaderEnquire">

                    <a href="/contact"><div class="homeHeaderButton">ENQUIRE NOW</div></a>

                </div>

            </div>


            <div id="servicesContent">

                <div id="servicesOurServices">

                    <h2 id="servicesOurServicesHeader">Our Services</h2>

                    <div class="centerUnderlineYellow"> </div>

                    <h3 id="servicesOurServicesText"><?php echo $servicesDetails[0]->ourServicesHeaderText; ?></h3>

                </div>

                <?php foreach ($services as $service) { ?>

                    <div class="servicesService" id="se<?php echo $service->id; ?>">

                        <div class="">

                            <h4 class="servicesServiceHeader"><img class="servicesServiceImage" src="<?php echo $service->imgURLPrimary; ?>" alt="<?php echo $service->serviceName; ?>"><?php echo $service->serviceName; ?></h4>

                        </div>

                        <h5 class="servicesServiceText"><?php echo $service->serviceText; ?></h5>

                    </div>

                <?php } ?>

            </div>

            <?php $curPage = "services"; ?>
            
            @include('includes/footer')