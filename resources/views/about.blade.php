<?php 

// ABOUT AGE DETAILS
$aboutDetails = DB::table('aboutpagedetails')->get();

// TEAM MEMBERS
$teamMembers = DB::table('teammembers')->get();

// PRINCIPLES
$principles = DB::table('principles')->get();

?>

<!doctype html>
<html lang="{{ app()->getLocale() }}">
    
@include('includes/header')

    <body>
       
        <div div="wrapper">

            @include('includes/menu')

            <!-- JQUERY -->
            <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

            <?php $curPage = "about"; ?>

            <?php switch($curPage) {

                case "about":
                ?>
                <script>
                    var menuEL = $("#aboutMenu");
                    menuEL.css('color', '#ee3126');
                    var offset = menuEL.offset();
                    var offsetLeft = menuEL.width() / 4;
                    $("#menuIcon").css("visibility", "visible");
                    $('#menuIcon').offset( {top: offset.top - 50, left: offset.left - ($("#menuIcon").width() / 2) + offsetLeft});
                </script>
                <?php
                break;
            }?>

           <div id="homeHeader">

                <img id="homeHeaderImage" src="<?php echo $aboutDetails[0]->imgURL; ?>">

                <div id="homeHeaderText">

                    <h2><?php echo $aboutDetails[0]->headerText; ?></h2>

                </div>


                <div id="homeHeaderEnquire">

                    <a href="/contact"><div class="homeHeaderButton">ENQUIRE NOW</div></a>

                </div>

            </div>

            <div id="aboutContent">

                <div id="aboutWhoAreWe">

                    <h2 id="aboutWhoAreWeHeader">Who Are We?</h2>

                    <div class="centerUnderlineYellow"> </div>

                    <h3 id="aboutWhoAreWeText"><?php echo $aboutDetails[0]->whoAreWeText; ?><h3>

                </div>

                <div id="aboutLeadershipTeam">

                    <h2 id="aboutLeadershipTeamHeader">Our Leadership Team</h2>

                    <div class="centerUnderlineYellow"> </div>

                    <h3 id="aboutLeadershipTeamText"><?php echo $aboutDetails[0]->leadershipTeamText; ?></h3>

                    <?php foreach ($teamMembers as $member) { ?>

                        <div class="aboutTeamMember">

                            <img class="aboutTeamMemberImage"src="<?php echo $member->imgURL; ?>" alt="<?php echo $member->teamMemberName; ?>">

                            <h4 class="aboutTeamMemberHeader"><?php echo $member->teamMemberName; ?></h4>

                            <h4 class="aboutTeamMemberRole"><?php echo $member->teamMemberPosition; ?></h4>

                            <h5 class="aboutTeamMemberText"><?php echo $member->teamMemberText; ?></h5>

                        </div>

                    <?php } ?>

                </div>

                <div id="aboutMission">

                    <h2 id="aboutMissionHeader">Our Mission</h2>

                    <div class="centerUnderlineYellow"> </div>

                    <h3 id="aboutMissiontext"><?php echo $aboutDetails[0]->ourMissionText; ?></h3>

                </div>

                <div id="aboutPrinciples">

                    <h2 id="aboutPrinciplesHeader">Our Principles</h2>

                    <div class="centerUnderlineYellow"> </div>

                    <?php foreach ($principles as $principle) { ?>

                        <div class="aboutPrinciple">

                            <img class="aboutPrincipleImage" src="<?php echo $principle->imgURL; ?>" alt="<?php echo $principle->principleName; ?>">

                            <h3 class="aboutPrincipleHeader"><?php echo $principle->principleName; ?></h3>

                        </div>

                    <?php } ?>

                </div>

            </div>

            
            
            @include('includes/footer')