<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<table>
		<tr>
			<td>Full Name : </td>
			<td>{!! $firstname !!} {!! $lastname !!}<td>
		</tr>
        <tr>
			<td>E-mail : </td>
			<td>{!! $email !!}</td>
        </tr>
        <tr>
			<td>Cell : </td>
			<td>{!! $number !!}</td>
		</tr>
        <tr>
			<td>Company Name : </td>
			<td>{!! $companyname !!}</td>
		</tr>
        <tr>
			<td>Subject : </td>
			<td>{!! $subject !!}</td>
		</tr>
		<tr>
			<td>Message : </td>
			<td>{{ $usermessage }}</td>
		</tr>
	</table>
</body>
</html>