<?php 

// GET PRODUCTS
$products = DB::table('products')->get();

$searchList = array();

foreach ($products as $product) {

    $curItem = array(
        "name" => "$product->productName",
        "type" => "Products",
        "urlPre" => "products",
        "id" => "$product->id"
    );

    array_push($searchList,$curItem);

}

$searchResults = [];

foreach ($searchList as $item) {

    if (strpos(strtolower($item['name']), strtolower($_GET['searchTerm'])) !== false) {
        array_push($searchResults, $item);
    }

}

echo json_encode($searchResults);

?>