<?php

    namespace App\Http\Controllers;

    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Redirect;
    use Illuminate\Support\Facades\File;
    use Mail;

    class LoginController extends Controller
    {

        // LOGIN TO CMS
        public function login(Request $request)
        {

            session_start();

            // VALIDATE FIELDS
            $validateFields = ([
                'username' => 'required',
                'password' => 'required'
            ]);
            $this->validate($request, $validateFields);

            // GET USERNAME
            $username = $_POST['username'];

            // GET PASSWORD
            $password = $_POST['password'];

            // GET PASS WITH ABOVE USERNAME
            $savedPass = DB::select("SELECT password FROM users where username='$username'");

            // IF NO MATCHES
            if (sizeof($savedPass) == 0) {
                echo "No user found with those credentials.";
            // IF 1 MATCH
            } else if (sizeof($savedPass) == 1) {
                
                // CHECK IF HASHED PASSES MATCH
                // PASS MATCH
                if (password_verify($password, $savedPass[0]->password)) {

                    // GET PASS WITH ABOVE USERNAME
                    $userDetails = DB::select("SELECT name, surname FROM users where username='$username'");

                    session(['name' => $userDetails[0]->name]);
                    session(['surname' => $userDetails[0]->surname]);
                    session(['loggedIn' => true]);
                    return redirect("cmshome");
                
                // PASS NOT MATCH
                } else {
                    return redirect("login");
                }

            // IF MULTIPLE MATCHES
            } else if (sizeof($savedPass) > 1) {
                echo "Error. Too many users found. Please contact administrator.";
            } 

        }

        public static function logout()
        {

            session()->flush();

            return redirect("login");
        }

        public function register(Request $request)
        {

            $hash = password_hash("testpass", PASSWORD_BCRYPT, ['cost' => 12]);

            DB::insert('INSERT INTO users (
                username, password) values (?, ?)',
            [
                "Francorider",
                $hash,

            ]);

        }

        // HOME PAGE EDIT
        public function dohomepagedetailsedit(Request $request) {

            // VALIDATE FIELDS
            $validateFields = ([
                'img' => 'image|mimes:jpeg,jpg,png,gif,svg',
                'pageheadertext' => 'required',
                'productsheadertext' => 'required',
                'servicesheadertext' => 'required',
                'differentiatorsheadertext' => 'required',
            ]);
            

            $this->validate($request, $validateFields);

            $pageheadertext = $request->pageheadertext;

            $productsheadertext = $request->productsheadertext;

            $servicesheadertext = $request->servicesheadertext;

            $differentiatorsheadertext = $request->differentiatorsheadertext;

            if ($file = $request->file('img')) {

                /*
                $name = time() . '-' . $file->getClientOriginalName();
                $file->move('images/uploads/', $name);
                $image_1_path = '/images/uploads/' . $name;
                $input['image_1'] = $image_1_path;

                */

                // GET IMAGE NAME
                $imageName = time() . $request->{'img'}->getClientOriginalName();

                // UPLOAD IMAGE TO FOLDER
                $upload = $request->{'img'}->move(public_path('/images/header'), $imageName);

                // GET URL OF OLD IMAGE
                $imgurl = DB::table('homepagedetails')->where('id', 1)->pluck('imgURL');

                // DELETE OLD IMAGE
                if (File::exists(public_path() . $imgurl[0])) {
                    File::delete(public_path() . $imgurl[0]);
                }

                // UPDATE RECORD
                DB::table('homepagedetails')
                ->where('id', 1)
                ->update([
                    'imgURL' => '/images/header/' . $imageName,
                    'headertext' => $pageheadertext,
                    'productsheadertext' => $productsheadertext,
                    'servicesheadertext' => $servicesheadertext,
                    'differentiatorsheadertext' => $differentiatorsheadertext,
                ]); 


            } else {


                // UPDATE RECORD
                DB::table('homepagedetails')
                ->where('id', 1)
                ->update([
                    'headertext' => $pageheadertext,
                    'productsheadertext' => $productsheadertext,
                    'servicesheadertext' => $servicesheadertext,
                    'differentiatorsheadertext' => $differentiatorsheadertext,
                ]); 

            }

            return redirect("homepagedetails");

        }

        // ABOUT PAGE EDIT
        public function doaboutpagedetailsedit(Request $request) {

            // VALIDATE FIELDS
            $validateFields = ([
                'img' => 'image|mimes:jpeg,jpg,png,gif,svg',
                'pageheadertext' => 'required',
                'whoareweheadertext' => 'required',
                'leadershipteamheadertext' => 'required',
                'ourmissionheadertext' => 'required',
            ]);
            

            $this->validate($request, $validateFields);

            $pageheadertext = $request->pageheadertext;

            $whoareweheadertext = $request->whoareweheadertext;

            $leadershipteamheadertext = $request->leadershipteamheadertext;

            $ourmissionheadertext = $request->ourmissionheadertext;

            if ($file = $request->file('img')) {

                /*
                $name = time() . '-' . $file->getClientOriginalName();
                $file->move('images/uploads/', $name);
                $image_1_path = '/images/uploads/' . $name;
                $input['image_1'] = $image_1_path;

                */

                // GET IMAGE NAME
                $imageName = time() . $request->{'img'}->getClientOriginalName();

                // UPLOAD IMAGE TO FOLDER
                $upload = $request->{'img'}->move(public_path('/images/header'), $imageName);

                // GET URL OF OLD IMAGE
                $imgurl = DB::table('aboutpagedetails')->where('id', 1)->pluck('imgURL');

                // DELETE OLD IMAGE
                if (File::exists(public_path() . $imgurl[0])) {
                    File::delete(public_path() . $imgurl[0]);
                }

                // UPDATE RECORD
                DB::table('aboutpagedetails')
                ->where('id', 1)
                ->update([
                    'imgURL' => '/images/header/' . $imageName,
                    'headertext' => $pageheadertext,
                    'whoAreWeText' => $whoareweheadertext,
                    'leadershipTeamText' => $leadershipteamheadertext,
                    'ourMissionText' => $ourmissionheadertext,
                ]); 


            } else {


                // UPDATE RECORD
                DB::table('aboutpagedetails')
                ->where('id', 1)
                ->update([
                    'headertext' => $pageheadertext,
                    'whoAreWeText' => $whoareweheadertext,
                    'leadershipTeamText' => $leadershipteamheadertext,
                    'ourMissionText' => $ourmissionheadertext,
                ]); 

            }

            return redirect("aboutpagedetails");

        }

        // PRODUCTS PAGE EDIT
        public function doproductspagedetailsedit(Request $request) {

            // VALIDATE FIELDS
            $validateFields = ([
                'img' => 'image|mimes:jpeg,jpg,png,gif,svg',
                'pageheadertext' => 'required',
                'productrangeheadertext' => 'required',
            ]);
            

            $this->validate($request, $validateFields);

            $pageheadertext = $request->pageheadertext;

            $productrangeheadertext = $request->productrangeheadertext;

            if ($file = $request->file('img')) {

                /*
                $name = time() . '-' . $file->getClientOriginalName();
                $file->move('images/uploads/', $name);
                $image_1_path = '/images/uploads/' . $name;
                $input['image_1'] = $image_1_path;

                */

                // GET IMAGE NAME
                $imageName = time() . $request->{'img'}->getClientOriginalName();

                // UPLOAD IMAGE TO FOLDER
                $upload = $request->{'img'}->move(public_path('/images/header'), $imageName);

                // GET URL OF OLD IMAGE
                $imgurl = DB::table('productspagedetails')->where('id', 1)->pluck('imgURL');

                // DELETE OLD IMAGE
                if (File::exists(public_path() . $imgurl[0])) {
                    File::delete(public_path() . $imgurl[0]);
                }

                // UPDATE RECORD
                DB::table('productspagedetails')
                ->where('id', 1)
                ->update([
                    'imgURL' => '/images/header/' . $imageName,
                    'headertext' => $pageheadertext,
                    'productRangeHeaderText' => $productrangeheadertext,

                ]); 


            } else {


                // UPDATE RECORD
                DB::table('productspagedetails')
                ->where('id', 1)
                ->update([
                    'headertext' => $pageheadertext,
                    'productRangeHeaderText' => $productrangeheadertext,

                ]); 

            }

            return redirect("productspagedetails");

        }

        // SERVICES PAGE EDIT
        public function doservicespagedetailsedit(Request $request) {

            // VALIDATE FIELDS
            $validateFields = ([
                'img' => 'image|mimes:jpeg,jpg,png,gif,svg',
                'pageheadertext' => 'required',
                'ourservicesheadertext' => 'required',
            ]);
            

            $this->validate($request, $validateFields);

            $pageheadertext = $request->pageheadertext;

            $ourservicesheadertext = $request->ourservicesheadertext;

            if ($file = $request->file('img')) {

                /*
                $name = time() . '-' . $file->getClientOriginalName();
                $file->move('images/uploads/', $name);
                $image_1_path = '/images/uploads/' . $name;
                $input['image_1'] = $image_1_path;

                */

                // GET IMAGE NAME
                $imageName = time() . $request->{'img'}->getClientOriginalName();

                // UPLOAD IMAGE TO FOLDER
                $upload = $request->{'img'}->move(public_path('/images/header'), $imageName);

                // GET URL OF OLD IMAGE
                $imgurl = DB::table('servicespagedetails')->where('id', 1)->pluck('imgURL');

                // DELETE OLD IMAGE
                if (File::exists(public_path() . $imgurl[0])) {
                    File::delete(public_path() . $imgurl[0]);
                }

                // UPDATE RECORD
                DB::table('servicespagedetails')
                ->where('id', 1)
                ->update([
                    'imgURL' => '/images/header/' . $imageName,
                    'headertext' => $pageheadertext,
                    'ourServicesHeaderText' => $ourservicesheadertext,

                ]); 

            } else {


                // UPDATE RECORD
                DB::table('servicespagedetails')
                ->where('id', 1)
                ->update([
                    'headertext' => $pageheadertext,
                    'ourServicesHeaderText' => $ourservicesheadertext,

                ]); 

            }

            return redirect("servicespagedetails");

        }

        // ADD TEAM MEMBER
        public function doteammemberadd(Request $request)
        {

            // VALIDATE FIELDS
            $validateFields = ([
                'teammembername' => 'required',
                'teammemberposition' => 'required',
                'teammembertext' => 'required',
                'img' => 'image|mimes:jpeg,jpg,png,gif,svg|required',
            ]);

            $this->validate($request, $validateFields);

            $teammembername = $request->teammembername;

            $teammemberposition = $request->teammemberposition;

            $teammembertext = $request->teammembertext;

             // INSERT DB RECORD
             $id = DB::table('teammembers')->insertGetId(
                ['teamMemberName' => $teammembername, 'teamMemberPosition' => $teammemberposition, 'teamMemberText' => $teammembertext]
            );

            // IF IMAGE PRESENT IN REQUEST
            if ($file = $request->file('img')){

                // GET IMAGE NAME
                $imageName = time() . $request->{'img'}->getClientOriginalName();
 
                // UPLOAD IMAGE TO FOLDER
                $upload = $request->{'img'}->move(public_path('/images/team'), $imageName);
 
                // GET URL OF OLD IMAGE
                $imgurl = DB::table('teammembers')->where('id', $id)->pluck('imgURL');
 
                // DELETE OLD IMAGE
                if (File::exists(public_path() . $imgurl[0])) {
                    File::delete(public_path() . $imgurl[0]);
                }     
 
                // UPDATE RECORD
                 DB::table('teammembers')
                 ->where('id', $id)
                 ->update([
                     'imgURL' => '/images/team/' . $imageName,
                 ]);
 
             }
        
            return redirect("teammembersview");
        
        }

        // EDIT TEAM MEMBER
        public function doteammemberedit(Request $request, $card)
        {

            // VALIDATE FIELDS
            $validateFields = ([
                'teammembername' => 'required',
                'teammemberposition' => 'required',
                'teammembertext' => 'required',
                'img' => 'image|mimes:jpeg,jpg,png,gif,svg',
            ]);

            $this->validate($request, $validateFields);

            // IF IMAGE PRESENT IN REQUEST
            if ($file = $request->file('img')){

               // GET IMAGE NAME
               $imageName = time() . $request->{'img'}->getClientOriginalName();

               // UPLOAD IMAGE TO FOLDER
               $upload = $request->{'img'}->move(public_path('/images/team'), $imageName);

               // GET URL OF OLD IMAGE
               $imgurl = DB::table('teammembers')->where('id', $card)->pluck('imgURL');

               // DELETE OLD IMAGE
               if (File::exists(public_path() . $imgurl[0])) {
                   File::delete(public_path() . $imgurl[0]);
               }     

               // UPDATE RECORD
                DB::table('teammembers')
                ->where('id', $card)
                ->update([
                    'imgURL' => '/images/team/' . $imageName,
                ]);

            }

            $teammembername = $request->teammembername;

            $teammemberposition = $request->teammemberposition;

            $teammembertext = $request->teammembertext;

            // UPDATE RECORD
            DB::table('teammembers')
            ->where('id', $card)
            ->update([
                'teamMemberName' => $teammembername,
                'teamMemberPosition' => $teammemberposition,
                'teamMemberText' => $teammembertext
            ]);
        
            return redirect("teammembersview");
        
        }

        // DELETE TEAM MEMBER
        public function doteammemberdelete(Request $request, $card)
        {

            // GET CARD IMAGES
            $teammember = DB::table('teammembers')->where('id', $card)->get();

            if (File::exists(public_path() . $teammember[0]->imgURL)) {
                File::delete(public_path() . $teammember[0]->imgURL);
            }

            // DELETE CARD RECORD
            DB::table('teammembers')->where('id', '=', $card)->delete();
            
            return redirect("teammembersview");
          
        }

        // ADD PRINCIPLE
        public function doprincipleadd(Request $request)
        {

            // VALIDATE FIELDS
            $validateFields = ([
                'principlename' => 'required',
                'img' => 'image|mimes:jpeg,jpg,png,gif,svg|required',
            ]);

            $this->validate($request, $validateFields);

            $principlename = $request->principlename;

            // GET IMAGE NAME
            $imageName = time() . $request->{'img'}->getClientOriginalName();

            // UPLOAD IMAGE TO FOLDER
            $upload = $request->{'img'}->move(public_path('/images/principles'), $imageName);

            // INSERT DB RECORD
            $id = DB::table('principles')->insertGetId(
                ['principleName' => $principlename, 'imgURL' => '/images/principles/' . $imageName]
            );

            return redirect("principlesview");
        
        }

        // EDIT PRINCIPLES
        public function doprincipleedit(Request $request, $card)
        {

            // VALIDATE FIELDS
            $validateFields = ([
                'principlename' => 'required',
                'img' => 'image|mimes:jpeg,jpg,png,gif,svg',
            ]);

            $this->validate($request, $validateFields);

            // IF IMAGE PRESENT IN REQUEST
            if ($file = $request->file('img')){

               // GET IMAGE NAME
               $imageName = time() . $request->{'img'}->getClientOriginalName();

               // UPLOAD IMAGE TO FOLDER
               $upload = $request->{'img'}->move(public_path('/images/principles'), $imageName);

               // GET URL OF OLD IMAGE
               $imgurl = DB::table('principles')->where('id', $card)->pluck('imgURL');

               // DELETE OLD IMAGE
               if (File::exists(public_path() . $imgurl[0])) {
                   File::delete(public_path() . $imgurl[0]);
               }     

               // UPDATE RECORD
                DB::table('principles')
                ->where('id', $card)
                ->update([
                    'imgURL' => '/images/principles/' . $imageName,
                ]);

            }

            $principlename = $request->principlename;

            // UPDATE RECORD
            DB::table('principles')
            ->where('id', $card)
            ->update([
                'principleName' => $principlename,
            ]);
        
            return redirect("principlesview");
        
        }

        // DELETE TEAM MEMBER
        public function doprinciplesdelete(Request $request, $card)
        {

            // GET CARD IMAGES
            $teammember = DB::table('principles')->where('id', $card)->get();

            if (File::exists(public_path() . $teammember[0]->imgURL)) {
                File::delete(public_path() . $teammember[0]->imgURL);
            }

            // DELETE CARD RECORD
            DB::table('principles')->where('id', '=', $card)->delete();
            
            return redirect("principlesview");
          
        }

        // ADD PRODUCT
        public function doproductadd(Request $request)
        {

            // VALIDATE FIELDS
            $validateFields = ([
                'productname' => 'required',
                'productapplication' => '',
                'productdescription' => '',
                'productbenefits' => '',
                'productfeatures' => '',
                'img1' => 'image|mimes:jpeg,jpg,png,gif,svg|required',
                'img2' => 'image|mimes:jpeg,jpg,png,gif,svg|required',
            ]);

            $this->validate($request, $validateFields);



            $productname = $request->productname;

            // GET IMAGE NAME
            $imageName1 = time() . $request->{'img1'}->getClientOriginalName();
            $imageName2 = time() . $request->{'img2'}->getClientOriginalName();

            // UPLOAD IMAGE TO FOLDER
            $upload = $request->{'img1'}->move(public_path('/images/products'), $imageName1);
            $upload = $request->{'img2'}->move(public_path('/images/products'), $imageName2);

            // INSERT DB RECORD
            $id = DB::table('products')->insertGetId(
                ['productName' => $productname, 'imgURLPrimary' => '/images/products/' . $imageName1, 'imgURLHome' => '/images/products/' . $imageName2]
            );

            if ($request->has('productapplication')) {

                $productapplication = $request->productapplication;

                // UPDATE RECORD
                DB::table('products')
                ->where('id', $id)
                ->update([
                    'productApplicationText' => $productapplication,
                ]);

            }

            if ($request->has('productdescription')) {

                $productdescription = $request->productdescription;

                // UPDATE RECORD
                DB::table('products')
                ->where('id', $id)
                ->update([
                    'productDescriptionText' => $productdescription,
                ]);

            }

            if ($request->has('productbenefits')) {

                $productbenefits = $request->productbenefits;

                // UPDATE RECORD
                DB::table('products')
                ->where('id', $id)
                ->update([
                    'productBenefitsText' => $productbenefits,
                ]);

            }

            if ($request->has('productfeatures')) {

                $productfeatures = $request->productfeatures;

                // UPDATE RECORD
                DB::table('products')
                ->where('id', $id)
                ->update([
                    'productFeaturesText' => $productfeatures,
                ]);

            }

            return redirect("productsview");
        
        }

        // EDIT PRODUCTS
        public function doproductedit(Request $request, $card)
        {

            // VALIDATE FIELDS
            $validateFields = ([
                'productname' => 'required',
                'productapplication' => '',
                'productdescription' => '',
                'productbenefits' => '',
                'productfeatures' => '',
                'img1' => 'image|mimes:jpeg,jpg,png,gif,svg',
                'img2' => 'image|mimes:jpeg,jpg,png,gif,svg',
            ]);

            $this->validate($request, $validateFields);

            // IF IMAGE PRESENT IN REQUEST
            if ($file = $request->file('img1')){

               // GET IMAGE NAME
               $imageName = time() . $request->{'img1'}->getClientOriginalName();

               // UPLOAD IMAGE TO FOLDER
               $upload = $request->{'img1'}->move(public_path('/images/products'), $imageName);

               // GET URL OF OLD IMAGE
               $imgurl = DB::table('products')->where('id', $card)->pluck('imgURLPrimary');

               // DELETE OLD IMAGE
               if (File::exists(public_path() . $imgurl[0])) {
                   File::delete(public_path() . $imgurl[0]);
               }     

               // UPDATE RECORD
                DB::table('products')
                ->where('id', $card)
                ->update([
                    'imgURLPrimary' => '/images/products/' . $imageName,
                ]);

            }

            // IF IMAGE PRESENT IN REQUEST
            if ($file = $request->file('img2')){

                // GET IMAGE NAME
                $imageName = time() . $request->{'img2'}->getClientOriginalName();
 
                // UPLOAD IMAGE TO FOLDER
                $upload = $request->{'img2'}->move(public_path('/images/products'), $imageName);
 
                // GET URL OF OLD IMAGE
                $imgurl = DB::table('products')->where('id', $card)->pluck('imgURLHome');
 
                // DELETE OLD IMAGE
                if (File::exists(public_path() . $imgurl[0])) {
                    File::delete(public_path() . $imgurl[0]);
                }     
 
                // UPDATE RECORD
                 DB::table('products')
                 ->where('id', $card)
                 ->update([
                     'imgURLHome' => '/images/products/' . $imageName,
                 ]);
 
             }

             if ($request->has('productapplication')) {

                $productapplication = $request->productapplication;

                // UPDATE RECORD
                DB::table('products')
                ->where('id', $card)
                ->update([
                    'productApplicationText' => $productapplication,
                ]);

            }

            if ($request->has('productdescription')) {

                $productdescription = $request->productdescription;

                // UPDATE RECORD
                DB::table('products')
                ->where('id', $card)
                ->update([
                    'productDescriptionText' => $productdescription,
                ]);

            }

            if ($request->has('productbenefits')) {

                $productbenefits = $request->productbenefits;

                // UPDATE RECORD
                DB::table('products')
                ->where('id', $card)
                ->update([
                    'productBenefitsText' => $productbenefits,
                ]);

            }

            if ($request->has('productfeatures')) {

                $productfeatures = $request->productfeatures;

                // UPDATE RECORD
                DB::table('products')
                ->where('id', $card)
                ->update([
                    'productFeaturesText' => $productfeatures,
                ]);

            }

            $productname = $request->productname;

            // UPDATE RECORD
            DB::table('products')
            ->where('id', $card)
            ->update([
                'productName' => $productname,
            ]);
        
            return redirect("productsview");
        
        }

        // DELETE PRODUCT
        public function doproductsdelete(Request $request, $card)
        {

            // GET CARD IMAGES
            $service = DB::table('products')->where('id', $card)->get();

            if (File::exists(public_path() . $service[0]->imgURLPrimary)) {
                File::delete(public_path() . $service[0]->imgURLPrimary);
            }

            if (File::exists(public_path() . $service[0]->imgURLHome)) {
                File::delete(public_path() . $service[0]->imgURLHome);
            }

            // DELETE CARD RECORD
            DB::table('products')->where('id', '=', $card)->delete();
            
            return redirect("productsview");
          
        }

        // ADD SERVICE
        public function doserviceadd(Request $request)
        {

            // VALIDATE FIELDS
            $validateFields = ([
                'servicename' => 'required',
                'servicetext' => 'required',
                'img1' => 'image|mimes:jpeg,jpg,png,gif,svg|required',
                'img2' => 'image|mimes:jpeg,jpg,png,gif,svg|required',
            ]);

            $this->validate($request, $validateFields);

            $servicename = $request->servicename;

            $servicetext = $request->servicetext;

            // GET IMAGE NAME
            $imageName1 = time() . $request->{'img1'}->getClientOriginalName();
            $imageName2 = time() . $request->{'img2'}->getClientOriginalName();

            // UPLOAD IMAGE TO FOLDER
            $upload = $request->{'img1'}->move(public_path('/images/services'), $imageName1);
            $upload = $request->{'img2'}->move(public_path('/images/services'), $imageName2);

            // INSERT DB RECORD
            $id = DB::table('services')->insertGetId(
                ['serviceName' => $servicename, 'serviceText' => $servicetext, 'imgURLPrimary' => '/images/services/' . $imageName1, 'imgURLSecondary' => '/images/services/' . $imageName2]
            );

            return redirect("servicesview");
        
        }

        // EDIT SERVICES
        public function doserviceedit(Request $request, $card)
        {

            // VALIDATE FIELDS
            $validateFields = ([
                'servicename' => 'required',
                'servicetext' => 'required',
                'img1' => 'image|mimes:jpeg,jpg,png,gif,svg',
                'img2' => 'image|mimes:jpeg,jpg,png,gif,svg',
            ]);

            $this->validate($request, $validateFields);

            // IF IMAGE PRESENT IN REQUEST
            if ($file = $request->file('img1')){

               // GET IMAGE NAME
               $imageName = time() . $request->{'img1'}->getClientOriginalName();

               // UPLOAD IMAGE TO FOLDER
               $upload = $request->{'img1'}->move(public_path('/images/services'), $imageName);

               // GET URL OF OLD IMAGE
               $imgurl = DB::table('services')->where('id', $card)->pluck('imgURLPrimary');

               // DELETE OLD IMAGE
               if (File::exists(public_path() . $imgurl[0])) {
                   File::delete(public_path() . $imgurl[0]);
               }     

               // UPDATE RECORD
                DB::table('services')
                ->where('id', $card)
                ->update([
                    'imgURLPrimary' => '/images/services/' . $imageName,
                ]);

            }

            // IF IMAGE PRESENT IN REQUEST
            if ($file = $request->file('img2')){

                // GET IMAGE NAME
                $imageName = time() . $request->{'img2'}->getClientOriginalName();
 
                // UPLOAD IMAGE TO FOLDER
                $upload = $request->{'img2'}->move(public_path('/images/services'), $imageName);
 
                // GET URL OF OLD IMAGE
                $imgurl = DB::table('services')->where('id', $card)->pluck('imgURLSecondary');
 
                // DELETE OLD IMAGE
                if (File::exists(public_path() . $imgurl[0])) {
                    File::delete(public_path() . $imgurl[0]);
                }     
 
                // UPDATE RECORD
                 DB::table('services')
                 ->where('id', $card)
                 ->update([
                     'imgURLSecondary' => '/images/services/' . $imageName,
                 ]);
 
             }

            $servicename = $request->servicename;

            $servicetext = $request->servicetext;

            // UPDATE RECORD
            DB::table('services')
            ->where('id', $card)
            ->update([
                'serviceName' => $servicename,
                'serviceText' => $servicetext
            ]);
        
            return redirect("servicesview");
        
        }

        // DELETE SERVICE
        public function doservicesdelete(Request $request, $card)
        {

            // GET CARD IMAGES
            $service = DB::table('services')->where('id', $card)->get();

            if (File::exists(public_path() . $service[0]->imgURLPrimary)) {
                File::delete(public_path() . $service[0]->imgURLPrimary);
            }

            if (File::exists(public_path() . $service[0]->imgURLSecondary)) {
                File::delete(public_path() . $service[0]->imgURLSecondary);
            }

            // DELETE CARD RECORD
            DB::table('services')->where('id', '=', $card)->delete();
            
            return redirect("servicesview");
          
        }
        

    }

?>