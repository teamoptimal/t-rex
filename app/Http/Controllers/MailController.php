<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\File;
use Mail;

class MailController extends Controller
{
    // CONTACT

    public function send(Request $request) {

        $validateFields = ([
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required',
            'number' => 'required',
            'companyname' => 'required',
            'subject' => '',
            'usermessage' => 'required'
        ]);

        $this->validate($request, $validateFields);

        $firstname = $_POST['firstname'];
        $lastname = $_POST['lastname'];
        $email = $_POST['email'];
        $number = $_POST['number'];
        $companyname = $_POST['companyname'];
        $subject = $_POST['subject'];
        $usermessage = $_POST['usermessage'];

        Mail::send('email/send', ['firstname' => $firstname, 'lastname' => $lastname, 'email' => $email, 'number' => $number, 'companyname' => $companyname, 'subject' => $subject, 'usermessage' => $usermessage], function ($message)
        {
            $message->from("info@trexmachinery.co.za", "T-Rex Machinery");

            $message->to("gero@meatec.co.za")->cc("steve@trexmachinery.co.za")->subject('A new message from T-Rex Machinery Website');
        });

    }
}
